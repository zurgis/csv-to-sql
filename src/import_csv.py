import tablib
import csv

from core.models import Country, Region, City


def import_counties():
    with open('../data/_countries.csv') as f:
        imported_data = tablib.Dataset().load(f)
        result = []

        for row in imported_data.dict:
            result.append(Country(
                id=row['id'],
                title_ru=row['title_ru'],
                title_en=row['title_en'],
                title_es=row['title_es']
            ))

        Country.objects.bulk_create(result)


def import_regions():
    with open('../data/_regions.csv') as f:
        imported_data = tablib.Dataset().load(f)
        result = []

        for row in imported_data.dict:
            result.append(Region(
                id=row['id'],
                title_ru=row['title_ru'],
                title_en=row['title_en'],
                title_es=row['title_es'],
                country_id=row['country']
            ))

        Region.objects.bulk_create(result)


def import_cities():
    with open('../data/_cities.csv') as f:
        reader = csv.reader(f)
        header = reader.__next__()
        result = []    
        cities = 0  # number of cities

        for row in reader:
            object_dict = {key: value for key, value in zip(header, row)}

            result.append(City(
                id=object_dict['city_id'],
                title_ru=object_dict['title_ru'],
                title_en=object_dict['title_en'],
                title_es=object_dict['title_es'],
                area_ru=object_dict['area_ru'],
                area_en=object_dict['area_en'],
                area_es=object_dict['area_es'],
                country_id=object_dict['country_id'],
                region_id=object_dict['region_id']
            ))
            # set batch size == 10000
            if len(result) == 10000:
                City.objects.bulk_create(result)
                result.clear()
                
                # find out how many cities are imported
                cities += 10000
                print(f'{cities} cities imported')

        City.objects.bulk_create(result)


print('Started import...')
import_counties()
print('Countries imported')
import_regions()
print('Regions imported')
import_cities()
print('Cities imported')
print('Import finished!')